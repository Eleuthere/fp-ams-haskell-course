module Lib where

data Vote = Yea | Nay
  deriving (Show)

instance Eq Vote where
  Yea == Yea = True
  Nay == Nay = True
  _ == _ = False

data Person = Person
  { name :: String,
    age :: Int
  }
  deriving (Eq, Show, Read)

mikeD = Person {name = "Michael Diamond", age = 43}

adRock = Person {name = "Adam Horovitz", age = 41}

mca = Person {name = "Adam Yauch", age = 44}

data Day
  = Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday
  | Saturday
  | Sunday
  deriving (Eq, Ord, Show, Read, Bounded, Enum)
