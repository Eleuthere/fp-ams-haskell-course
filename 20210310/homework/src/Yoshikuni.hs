{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}

module Yoshikuni where

import ExprT
import Parser
import StackVM

-- Exercise 1
eval :: ExprT -> Integer
eval expression = case expression of
    Lit int -> int
    ExprT.Add left right -> eval left + eval right
    ExprT.Mul left right -> eval left * eval right

-- Exercise 2
evalStr :: String -> Maybe Integer
evalStr string = eval <$> parseExp Lit ExprT.Add ExprT.Mul string 
-- fmap eval parseExp Lit Add Mul string 

-- Exercise 3
class Expr a where
    lit :: Integer -> a
    add :: a -> a -> a
    mul :: a -> a -> a

instance Expr ExprT where
    lit = Lit
    add = ExprT.Add
    mul = ExprT.Mul

-- Exercise 4
instance Expr Integer where
    lit = id
    add = (+)
    mul = (*)

instance Expr Bool where
    lit = (>0)
    add = (||)
    mul = (&&)

newtype MinMax = MinMax Integer deriving (Eq, Show)

instance Expr MinMax where
    lit = MinMax
    add (MinMax lhs) (MinMax rhs) = lit $ max lhs rhs
    mul (MinMax lhs) (MinMax rhs) = lit $ min lhs rhs

newtype Mod7 = Mod7 Integer deriving (Eq, Show)

instance Expr Mod7 where
    lit value = Mod7 $ value `mod` 7
    add (Mod7 lhs) (Mod7 rhs) = lit $ lhs + rhs
    mul (Mod7 lhs) (Mod7 rhs) = lit $ lhs * rhs

-- Exercise 5

instance Expr Program where
    lit value = [PushI value]
    add lhs rhs = lhs ++ rhs ++ [StackVM.Add]
    mul lhs rhs = lhs ++ rhs ++ [StackVM.Mul]

compile :: String -> Maybe Program 
compile  = parseExp lit add mul
