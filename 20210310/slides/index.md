# Haskell: From Beginner to Intermediate #5

## FP AMS 10/03/2021 

### Defining Your Own Types - Part 2

---

# Goal

![inline](learning-curve.png)

---

# Overall Planning

1. **Basics**
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)

---

# Last Week

## Defining Your Own Types and Typeclasses

### Part 1

- Algebraic data types
- Record syntax
- Type parameters

---

# This Week
  
## Defining Your Own Types and Typeclasses 

### Part 2

- Typeclasses and derived instances
- Type synonyms
- Recursive data structures

---

# Homework (1/10)

```
I 6 Completed armadillo processing
I 1 Nothing to report
I 4 Everything normal
I 11 Initiating self-destruct sequence
E 70 3 Way too many pickles
E 65 8 Bad pickle-flange interaction detected
W 5 Flange is due for a check-up
I 7 Out for lunch, back in two time steps
E 20 2 Too many pickles
I 9 Back from lunch
```

---

# Homework (2/10)

```haskell
data MessageType
  = Info
  | Warning
  | Error Int
  deriving (Show, Eq)

type TimeStamp = Int

data LogMessage
  = LogMessage MessageType TimeStamp String
  | Unknown String
  deriving (Show, Eq)
```

---

# Homework (3/10)

**Exercise 1** Parse individual messages, helpers:

```haskell
type ParseResult a = Maybe (a, String)

parseMessageType :: String -> ParseResult MessageType
parseMessageType msg =
  case words msg of
    ("I" : ws) -> Just (Info, unwords ws)
    ("W" : ws) -> Just (Warning, unwords ws)
    ("E" : ws) -> case parseInt (unwords ws) of
      Just (n, msg') -> Just (Error n, msg')
      Nothing -> Nothing
    _ -> Nothing

parseInt :: String -> ParseResult Int
parseInt s = case words s of
  [] -> Nothing
  (w : ws) -> case readMaybe w :: Maybe Int of
    Just n -> Just (n, unwords ws)
    Nothing -> Nothing

```

---

# Homework (4/10)

**Exercise 1** Parse individual messages:

```haskell
parseMessage :: String -> LogMessage
parseMessage msg = case parseMessageType msg of
  Nothing -> Unknown msg
  Just (msgType, msg') -> case parseInt msg' of
    Just (n, msg'') -> LogMessage msgType n msg''
    Nothing -> Unknown msg
```

Parse an entire log file:

```haskell
parse :: String -> [LogMessage]
parse = map parseMessage . lines
```

---

# Homework (5/10)

**Exercise 1** Parse individual messages, alternative using monads:

```haskell
parseMessage' :: String -> Maybe LogMessage
parseMessage' msg = do
  (msgType, msg') <- parseMessageType msg
  (n, msg'') <- parseInt msg'
  pure $ LogMessage msgType n msg''
```

---

# Homework (6/10)

Putting the logs in order.

```haskell
data MessageTree
  = Leaf
  | Node MessageTree LogMessage MessageTree
  deriving (Show, Eq)
```

Note:

- A `MessageTree` should be sorted by timestamp
- `Unknown` messages should not be stored in a `MessageTree`

---

# Homework (7/10)

**Exercise 2** Define a function which inserts a new `LogMessage` into an existing
`MessageTree`:

```haskell
insert :: LogMessage -> MessageTree -> MessageTree
insert (Unknown _) tree = tree
insert msg Leaf = Node Leaf msg Leaf
insert
  msg@(LogMessage _ t1 _)
  (Node left middle@(LogMessage _ t2 _) right)
    | t1 < t2 = Node (insert msg left) middle right
    | t1 >= t2 = Node left middle (insert msg right)
```

---

# Homework (8/10)

**Exercise 3** Define a function which given a list of messages builds up an
complete `MessageTree`:

```haskell
build :: [LogMessage] -> MessageTree
build = foldr insert Leaf
```

---

# Homework (9/10)

**Exercise 4** Define a function which takes a sorted `MessageTree` and produces
a list of all the `LogMessage`s it contains, sorted by timestamp from smallest
to biggest:

```haskell
inOrder :: MessageTree -> [LogMessage]
inOrder Leaf = []
inOrder (Node left msg right) =
  inOrder left ++ [msg] ++ inOrder right
```

---

# Homework (10/10)

**Exercise 5** Write a function which takes an unsorted list of `LogMessage`s,
and returns a list of the messages corresponding to any errors with a severity
of 50 or greater, sorted by timestamp:

```haskell
whatWentWrong :: [LogMessage] -> [String]
whatWentWrong = map content . filter isSevere . inOrder . build
  where
    isSevere :: LogMessage -> Bool
    isSevere (LogMessage (Error n) _ _) = n > 50
    isSevere _ = False
    content :: LogMessage -> String
    content (LogMessage _ _ s) = s
```

---

# Typeclasses and Derived Instances (1/4)

## Typeclass Declaration

```haskell
class Eq a where
  (==) :: a -> a -> Bool
  (/=) :: a -> a -> Bool
  x == y = not (x /= y)
  x /= y = not (x == y)
```

---

# Typeclasses and Derived Instances (2/4)

## Instance Declaration

```haskell
data Vote = Yea | Nay

instance Eq Vote where
  Yea == Yea = True
  Nay == Nay = True
  _ == _ = False
```

---

# Typeclasses and Derived Instances (3/4)

## `Eq`, `Show`, and `Read` Typeclass

```haskell
data Person = Person
  { name :: String,
    age :: Int
  } Deriving (Eq, Show, Read)
```

---

# Typeclasses and Derived Instances (4/4)

## `Ord`, `Bounded`, and `Enum` Typeclass

```haskell
data Day
  = Monday
  | Tuesday
  | Wednesday
  | Thursday
  | Friday
  | Saturday
  | Sunday
  deriving (Eq, Show, Read, Ord, Bounded, Enum)
```

---

# Type Synonyms

```haskell
type String = [Char]
```

Parameterized:

```haskell
type ParseResult a = Maybe (a, String)
```

---

# [fit] Next Week:

# [fit] Functors

---

# Homework

Will be posted on [slack](https://join.slack.com/t/fp-ams/shared_invite/zt-mdl0ok4g-PznibhXToWSR93ilvbI_Xw)
