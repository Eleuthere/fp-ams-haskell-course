# Haskell: From Beginner to Intermediate #9

## Applicative Functors

### FP AMS 07/04/2021 

---

![right 100%](learning-curve.png)

# Overall Planning

1. Basics
2. Functors, **Applicative Functors**, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)![inline 64%](book-of-monads.png)

---

# Applicative Typeclass

```haskell
class Functor f => Applicative f where
  pure :: a -> f a
  (<*>) :: f (a -> b) -> f a -> f b 
  liftA2 :: (a -> b -> c) -> f a -> f b -> f c
```

Note: we're mainly ignoring `liftA2`

---

![](source.gif)

---

# Defining `liftA2` with `<$>` and `<*>` (1/4)

```haskell
liftA2 :: Applicative f => (a -> b -> c) -> f a -> f b -> f c
liftA2 f x y = f <$> x <*> y
```

---

# Defining `liftA2` with `<$>` and `<*>` (2/4)

`(<$>)` and `(<*>)`:
- have the same precedence (4)
- are both left-associative

Thus if we add parentheses:

- `(f <$> x) <*> y`

---

# Defining `liftA2` with `<$>` and `<*>` (3/4)

Let's follow the types:

- `f :: a -> b -> c` 
- `a -> b -> c = a -> (b -> c)`
- `(<$>) :: (a -> b) -> f a -> f b`
- `(<$>) :: (a -> (b -> c)) -> f a -> f (b -> c)`
- `(f <$>) :: f a -> f (b -> c)`

---

# Defining `liftA2` with `<$>` and `<*>` (4/4)

Let's follow the types:

- `(f <$>) :: f a -> f (b -> c)`
- `x :: f a`
- `(f <$> x) :: f (b -> c)`
- `<*> :: f (a -> b) -> f a -> f b`
- `<*> :: f (b -> c) -> f b -> f c`
- `y :: f b`
- `(f <$> x <*> y) :: f c`

---

# Maybe Instance

```haskell
instance Applicative Maybe where
  pure = Just
  Nothing  <*> _ = Nothing
  (Just f) <*> x = fmap f x
```

---

# [] Instance

```haskell
instance Applicative [] where
  pure x = [x]
  fs <*> xs = [f x | f <- fs, x <- xs]
```

---

# Homework

```haskell
-- A parser for a value of type a is a function which takes a String
-- representing the input to be parsed, and succeeds or fails; if it
-- succeeds, it returns the parsed value along with the remainder of
-- the input.

newtype Parser a = Parser {runParser :: String -> Maybe (a, String)}
```
