module DmitriiOrlov where

{- Exercise 1:

    a. Prove `Functor (Either a)` satisfies the 1st Functor law.
    b. Prove `Functor []` satisfies the 1st Functor law.

    1st law: `fmap id = id` for all Functor instances
 -}

-- SOLUTION:

-- a. By case distinction of two possible data values for `Either a`: 
--      Left X` for some `X :: a`,
--      fmap id (Left X) = Left (id X) = Left X
--      id (Left X) = Left X
--  and
--      `Right Y` for some `Y :: b`,
--      fmap id (Right X) = Right (id X) = Right X
--      id (Right X) = Right X
-- It shows that the 1st law holds for `Either a`

-- b. By induction proof on the structure of a list `xs :: [a]` for any `a`:
--    With
--      as `map` is the `fmap` for Functor of Lists...
--      the base case xs == []:
--          fmap id xs == fmap id [] == map id [] == [] 
--          id xs == id [] == []
--      and for the step case `xs' == xs ++ [x'] == (++) xs [x']
--          fmap id xs' == fmap id ((++) xs [x']) == map id ((++) xs [x'])
--                          == (++) (map id xs) (id [x']) == (++) xs [x'] == xs'
--          id xs' == xs'
--  Thus the 1st Functor low holds for `List a`


{- Exercise 2:

    a. Prove `Functor (Either a)` satisfies the 2nd Functor law.
    b. Prove `Functor []` satisfies the 2nd Functor law.

    2nd law: `fmap (f . g) = fmap f . fmap g` for all Functor instances
 -}

-- SOLUTION:

-- a. By case distinction of two possible data values for `Either a`: 
--      Left X` for some `X :: a` and some functions `f` and `g`
--      fmap (f . g) (Left X) == Left (f . g) X == Left ( f ( g X))
--      (fmap f . fmap g) (Left X) == fmap f (fmap g (Left X))
--                                 == fmap f (Left (g X))
--                                 == Left (f (g X))
--  and
--      `Right Y` for some `Y :: b` and some functions `f` and `g`
--      fmap (f . g) (Right Y) == Right (f . g) Y == Right ( f ( g Y))
--      (fmap f . fmap g) (Right Y) == fmap f (fmap g (Right Y))
--                                  == fmap f (Right (g Y))
--                                  == Right (f (g Y))
-- It shows that the 2nd law holds for `Either a`

-- b. By induction proof on the structure of a list `xs :: [a]` for any `a`:
--    With
--      as `map` is the `fmap` for Functor of Lists...
--      the base case xs == []:
--          fmap (f . g) xs == fmap (f . g) [] == map (f . g) [] == []
--          (fmap f . fmap g) xs == fmap f (fmap g []) == map f (map g []) == map f [] == []
--      and for the step case `xs' == xs ++ [x'] == (++) xs [x']
--          fmap (f . g) xs' == fmap (f . g) ((++) xs [x']) == map (f . g) ((++) xs [x'])
--                           == ((++) (map (f . g) xs) ([(f . g) x'])
--          (fmap f . fmap g) xs' == (fmap f . fmap g) ((++) xs [x'])
--                                == fmap f (fmap g ((++) xs [x']))
--                                == fmap f ((++) (map g xs) (map g [x']))
--                                == map f ((++) (map g xs) (map g [x']))
--                                == (++) (map f (map g xs)) (map f (map g [x']))
--                                == (++) (map (f . g) xs) (map (f . g) [x'])
--                                == (++) (map (f . g) xs) ([(f . g) x'])
--  Thus the 2nd Functor low holds for `List a`


{- Exercise 3:

    Show that for instance below Functor laws don't hold.

    ~
        data CMaybe a = CNothing | CJust Int a
                        deriving (Show)
        instance Functor CMaybe where
            fmap f CNothing = CNothing
            fmap f (CJust counter x) = CJust (counter +1) (f x)
    ~
 -}

-- SOLUTION

-- 1st Functor law: `fmap id = id`
-- By proof by negation, we can show that the law does not hold for `CMaybe a`:
--  for `X = CNothing`:  
--      fmap id CNothing == CNothing == id CNothing
--  but for `X' = CJust n p` for some `Int n` and a value `p` of some type `a`
--      fmap id (CJust n p) == CJust (n + 1) (id p)
--      id (CJust n p) == CJust n p !== CJust (n + 1) (id p)
--  Therefore, since in one of the cases the law does not hold,
--      the law does not hold in general.

-- 2nd Functor lwa: `fmap (f . g) == fmap f . fmap g`
-- Again, using the proof by negation, we can show that this law does not hold:
--  for `X = CNothing`:
--      fmap (f . g) CNothing == CNothing
--      ((fmap f) . (fmap g)) CNothning == fmap f (fmap g CNothing)
--                                      == fmap f CNothing == CNothing
--  and for `X' = CJust n p` for some `Int n` and a value `p` of some type `a`
--      fmap (f . g) (CJust n p) == CJust (n + 1) ((f . g) p)
--      ((fmap f) . (fmap g)) CJust n p == fmap f (fmap g (CJust n p))
--                                      == fmap f (CJust (n + 1) (g p))
--                                      == CJust (n + 2) (f (g p))
--                                      == CJust (n + 2) ((f . g) p)
--  Therefore as CJust (n + 1) ((f . g) p) !== CJust (n + 2) ((f . g) p),
--    the 2nd law does not hold for the second case and thus
--    does not hold in general.