-- From homework list https://www.cis.upenn.edu/~cis194/spring13/hw/04-higher-order.pdf
-- Exercise 1:

fun1 :: [Integer] -> Integer
fun1 [] = 1
fun1 (x : xs)
  | even x = (x - 2) * fun1 xs
  | otherwise = fun1 xs

fun1' :: [Integer] -> Integer
fun1' = foldProduct . mapMinus2 . filterEven
  where
    filterEven = filter even
    mapMinus2 = map ((-) 2)
    foldProduct = foldl (*) 1

fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n
  | even n = n + fun2 (n `div` 2)
  | otherwise = fun2 (3 * n + 1)

fun2' :: Integer -> Integer
fun2' = foldl (+) 0
          . filter even
          . takeWhile (> 1)
          . iterate (\x -> if even x then x `div` 2 else 3 * x + 1)

-- Exercise 3:

-- part 1
xor :: [Bool] -> Bool
xor = foldl ((==) . not) False

-- part 2
map' :: (a -> b) -> [a] -> [b]
map' f = foldr (\x -> ([f x] ++)) []

-- part 3
myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl f base xs = foldr (flip f) base (reverse xs)