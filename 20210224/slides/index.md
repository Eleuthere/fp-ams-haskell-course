# Haskell: From Beginner to Intermediate #3

## FP AMS 24/02/2021 

---

# Goal

![inline](learning-curve.png)

---

# Overall Planning

1. **Basics**
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)

---

# Last Week

- Function syntax
- Recursion
- Modules

---

# This Week

- Currying
- Functions that take functions as parameters
- Functions that return functions
- Maps and filters
- Lambdas
- Folds
- Function application with `$`
- Function composition

---

# Currying

![right](haskell-curry.png)

- `a -> b -> c`
- `a -> (b -> c)`
- `a -> a -> a -> a`
- `a -> (a -> (a -> a))`
- schönfinkeling

---

![inline](something-different.png)

---

# A Quick Rant on Naming

## Descriptive Names

- "mappable" instead of "functor"
- underlying concept
- (historical) context

---

# Function-Taking Functions

```haskell
applyTwice :: (a -> a) -> a -> a
applyTwice f x = f (f x)
```

Note that `->` is right-associative

## Partial Application

```
λ> plus6 = applyTwice (+ 3)
λ> plus6 10
16
```

---

# Function-Taking Functions

## zipWith

```haskell
zipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
zipWith _ [] _ = []
zipWith _ _ [] = []
zipWith f (x : xs) (y : ys) = f x y : zipWith f xs ys
```

---

# Function-Returning Functions

```haskell
flip :: (a -> b -> c) -> (b -> a -> c)
flip f = g
  where
    g x y = f y x
```

---

# Map

```haskell
map :: (a -> b) -> [a] -> [b]
map _ [] = []
map f (x : xs) = f x : map f xs
```

---

# Filter

```haskell
filter :: (a -> Bool) -> [a] -> [a]
filter _ [] = []
filter p (x : xs)
  | p x = x : filter p xs
  | otherwise = filter p xs
```

---

# Filter

## Quicksort

```haskell
quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x : xs) =
  let smallerSorted = quicksort (filter (<= x) xs)
      biggerSorted = quicksort (filter (> x) xs)
   in smallerSorted ++ [x] ++ biggerSorted
```
---

# Lambdas

```haskell
\ x y -> x + y
```

---

# Folds

## Left Fold

```haskell
foldl :: (b -> a -> b) -> b -> [a] -> b

sum :: (Num a) => [a] -> a
sum xs = foldl (\acc x -> acc + x) 0 xs
```

---

# Folds

## Left Fold

```haskell
foldl :: (b -> a -> b) -> b -> [a] -> b
foldl f x [] = x
foldl f x (y : ys) = foldl f (f x y) ys
```

---

# Folds

## Right Fold

```haskell
foldr :: (a -> b -> b) -> b -> [a] -> b
```

## Fold[lr]1

```haskell
foldl1 :: (a -> a -> a) -> t a -> a
foldr1 :: (a -> a -> a) -> t a -> a
```

---

# Function Application with `$`

```haskell
($) :: (a -> b) -> a -> b
f $ x = f x

sum (filter (> 10) (map (*2) [2..10]))

sum $ filter (> 10) $ map (*2) [2..10]
```

---

# Function Composition

```haskell
(.) :: (b -> c) -> (a -> b) -> a -> c  
g . f = \x -> g (f x) 

map (\x -> negate (abs x)) [5,-3,-6,7,-3,2,-19,24]

map (negate . abs) [5,-3,-6,7,-3,2,-19,24]
```

---

# Next Week

- Defining types and typeclasses

---

# Homework

[https://www.cis.upenn.edu/~cis194/spring13/hw/04-higher-order.pdf](https://www.cis.upenn.edu/~cis194/spring13/hw/04-higher-order.pdf)

---

# References

- [Haskell Curry biography](https://mathshistory.st-andrews.ac.uk/Biographies/Curry/)
