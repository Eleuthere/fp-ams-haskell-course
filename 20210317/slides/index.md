# Haskell: From Beginner to Intermediate #6

## The `Functor` Typeclass

### FP AMS 17/03/2021 

---

# Goal

![inline](learning-curve.png)

---

# Overall Planning

1. Basics
2. **Functors**, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)

---

# Last Week
  
## Defining Your Own Types and Typeclasses 

### Part 2

- Typeclasses and derived instances
- Type synonyms
- Recursive data structures

---

# This Week:

# [fit] Functors

---

# Homework (1/14)

> On day one of your new job as a software engineer, you’ve been asked to
> program the brains of the company’s new blockbuster product: a calculator.
> But this isn’t just any calculator! Extensive focus group analysis has
> revealed that what people really want out of their calculator is something
> that can add and multiply integers. Anything more just clutters the interface.

Domain model:

```haskell
data ExprT
  = Lit Integer
  | Add ExprT ExprT
  | Mul ExprT ExprT
  deriving (Show, Eq)
```

---

# Homework (2/14)

**Exercise 1** Write Version 1 of the calculator: an evaluator for ExprT, with
the signature `eval :: ExprT -> Integer`.

```haskell
eval :: ExprT -> Integer
eval (Lit n) = n
eval (Add e1 e2) = eval e1 + eval e2
eval (Mul e1 e2) = eval e1 * eval e2
```

---

# Homework (3/14)

**Exercise 2** Given `parseExp` write `evalStr` which evaluates arithmetic
expressions given as a `String`

```haskell
-- Parse an arithmetic expression using the supplied semantics for
-- integral constants, addition, and multiplication.
parseExp :: (Integer -> a) -> (a -> a -> a) -> (a -> a -> a) -> String -> Maybe a
```

Solution:

```haskell
evalStr :: String -> Maybe Integer
evalStr s = case parseExp Lit Add Mul s of
  Nothing -> Nothing
  Just e -> Just (eval e)
```

---

# Homework (4/14)

**Exercise 2**

Alternative solution (after today):

```haskell
evalStr :: String -> Maybe Integer
evalStr = fmap eval . parseExp Lit Add Mul
```

---

# Homework (5/14)

**Exercise 3** Create a type class called `Expr` with three methods called `lit`, 
`add`, and `mul` which parallel the constructors of `ExprT`. Make an instance of
`Expr` for the `ExprT` type, in such a way that 

```
mul (add (lit 2) (lit 3)) (lit 4) :: ExprT
   == Mul (Add (Lit 2) (Lit 3)) (Lit 4)
```

Hint: consider the types of the `ExprT constructors

```
λ> :t Lit
Lit :: Integer -> ExprT
λ> :t Add
Add :: ExprT -> ExprT -> ExprT
λ> :t Mul
Mul :: ExprT -> ExprT -> ExprT
```

---

# Homework (6/14)

**Exercise 3**

Typeclass:

```haskell
class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a
```

`ExprT` instance:

```haskell
instance Expr ExprT where
  lit = Lit
  add = Add
  mul = Mul
```

---

# Homework (7/14)

**Exercise 4** Make instances of Expr for each of the following types:

- `Integer` - works like the original calculator
- `Bool` - every literal value less than or equal to 0 is interpreted as
`False`, and all positive Integers are interpreted as `True`; “addition” is 
logical or, “multiplication” is logical and
- `MinMax` — “addition” is taken to be the max function, while “multiplication”
is the min function
- `Mod7` — all values should be in the range from 0 to 6, and all arithmetic is
done modulo 7; for example, 5 + 3 = 1

---

# Homework (8/14)

**Exercise 4** `Integer`, works like the original calculator

```haskell
class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

instance Expr Integer where
  lit = id
  add = (+)
  mul = (*)
```

---

# Homework (9/14)

**Exercise 4** `Bool`, every literal value less than or equal to 0 is
interpreted as `False`, and all positive Integers are interpreted as `True`;
“addition” is logical or, “multiplication” is logical and

```haskell
class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

instance Expr Bool where
  lit n = n > 0
  add = (||)
  mul = (&&)
```

---

# Homework (10/14)

**Exercise 4** `MinMax`, “addition” is taken to be the `max` function, while
“multiplication” is the `min` function

```haskell
class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

newtype MinMax = MinMax Integer deriving (Eq, Show)

instance Expr MinMax where
  lit = MinMax
  add (MinMax x) (MinMax y) = MinMax $ max x y
  mul (MinMax x) (MinMax y) = MinMax $ min x y
```

---

# Homework (11/14)

**Exercise 4** `Mod7`, all values should be in the range from 0 to 6, and all
arithmetic is done modulo 7; for example, 5 + 3 = 1

```haskell
class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

newtype Mod7 = Mod7 Integer deriving (Eq, Show)

instance Expr Mod7 where
  lit n = Mod7 (n `mod` 7)
  add (Mod7 x) (Mod7 y) = lit $ x + y
  mul (Mod7 x) (Mod7 y) = lit $ x * y
```

---

# Homework (12/14)

**Exercise 5** The hardware group has provided you with `StackVM.hs`, which is a
software simulation of the custom CPU. The CPU supports six operations, as
embodied in the StackExp data type:

```haskell
data StackExp
  = PushI Integer
  | PushB Bool
  | Add
  | Mul
  | And
  | Or
  deriving (Show)

type Program = [StackExp]
```

---

# Homework (13/14)

**Exercise 5** Your task is to implement a compiler for arithmetic expressions.
Simply create an instance of the `Expr` type class for `Program`, so that 
arithmetic expressions can be interpreted as compiled programs.

```haskell
class Expr a where
  lit :: Integer -> a
  add :: a -> a -> a
  mul :: a -> a -> a

instance Expr StackVM.Program where
  lit n = [StackVM.PushI n]
  add p1 p2 = p1 ++ p2 ++ [StackVM.Add]
  mul p1 p2 = p1 ++ p2 ++ [StackVM.Mul]
```

---

# Homework (14/14)

**Exercise 5** Finally, put together the pieces you have to create a function
which takes `String`s representing arithmetic expressions and compiles them 
into programs that can be run on the custom CPU.

```haskell
compile :: String -> Maybe StackVM.Program
compile = parseExp lit add mul
```

---

# The `Functor` Typeclass

## Strategy for Learning

> The **pincer movement**, or **double envelopment**, is a military maneuver in
> which forces simultaneously attack both flanks (sides) of an enemy formation.
-- Wikipedia

1. analogies
2. "suspension of disbelief"
3. ~~category theory~~

---

# The `Functor` Typeclass

```haskell
class Functor f where
  fmap :: (a -> b) -> f a -> f b
```

Note: `f` is not a concrete type, but a type constructor

---

# The `Functor` Typeclass

Analogies:

- box
- container
- context
- "things that can be mapped over"

---

# List As a Functor

Remember `map`? 

```haskell
fmap :: (a -> b) -> [a] -> [b]

instance Functor [] where
  fmap = map
```

Note: type constructor `[]`

---

# Maybe As a Functor

```haskell
fmap :: (a -> b) -> Maybe a -> Maybe b

instance Functor Maybe where
  fmap f Nothing  = Nothing
  fmap f (Just x) = Just (f x)
```

---

# Tree As a Functor

```haskell
data Tree a
  = Leaf
  | Node a (Tree a) (Tree a)
  deriving (Show)
  
fmap :: (a -> b) -> Tree a -> Tree b

instance Functor Tree where
  fmap f Leaf = Leaf
  fmap f (Node x l r) = Node (f x) (fmap f l) (fmap f r)
```

---

# Either As a Functor

```haskell
data Either a b = Left a | Right b 

fmap :: (b -> c) -> Either a b -> Either a c

instance Functor (Either a) where
  fmap f (Left x) = Left x
  fmap f (Right x) = Right (f x)
```

---

# Next Week:

- Functors
- Functor laws
- Applicative functors

---

# Homework

Make `(->) r` an instance of `Functor`

Hints:

- `(->) r` is a type constructor for all unary functions that take a value of
type `r` as their argument
- Try to come up with the type of `fmap` for this instance first, 
  `fmap :: (a -> b) -> ... -> ...`
