module Yoshikuni where

import GHC.Base hiding (Functor, fmap)


class Functor f where 
    fmap :: (a -> b) -> f a -> f b

instance Yoshikuni.Functor ((->) r) where
    fmap morphism originalFunction = morphism . originalFunction


intToString :: Int -> String
intToString = show 

stringLength :: String -> Int 
stringLength = length

newFunction :: Int -> Int
newFunction = Yoshikuni.fmap stringLength intToString

isNumberOfLettersEven :: Int -> Bool
-- isNumberOfLettersEven = Yoshikuni.fmap even (Yoshikuni.fmap stringLength intToString)
isNumberOfLettersEven = even <$> stringLength <$> intToString