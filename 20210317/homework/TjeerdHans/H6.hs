--{-# LANGUAGE NoImplicitPrelude #-}
module Homework6 where

-- Tree stuff
data Tree a
  = Leaf
  | Node a (Tree a) (Tree a)
  deriving (Show)
  
--fmap :: (a -> b) -> Tree a -> Tree b

instance Functor Tree where
  fmap f Leaf = Leaf
  fmap f (Node x l r) = Node (f x) (fmap f l) (fmap f r)

testTreeFmap = fmap (+1) (Node 4 (Node 4 Leaf Leaf) Leaf)

-- using my own functor class name to not collide with Prelude
class THFunctor f where
   thfmap :: (a -> b) -> f a -> f b 


-- fmap :: (a -> b) -> (r -> a) -> (r -> b)
--         function from a to b
--                     unary function from r to a
--                                 unary function from r to b
instance THFunctor ((->) r) where
    thfmap f u = f . u

-- put the result of u (a) into f, which returns b, and so results in r->b
-- ('u' stands for 'unary')
testThFmap = thfmap (+1) (+1) 1