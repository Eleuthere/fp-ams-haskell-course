module Yoshikuni where

import Data.Char
import Control.Applicative

newtype Parser a =  Parser { runParser :: String -> Maybe(a, String) }

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser f
    where 
        f [] = Nothing 
        f (x:xs)
            | p x = Just (x, xs)
            | otherwise  = Nothing

char :: Char -> Parser Char 
char c = satisfy (== c)

posInt :: Parser Integer 
posInt = Parser f
    where
        f xs
            | null ns = Nothing 
            | otherwise = Just (read ns, rest)
            where (ns, rest) = span isDigit xs

-- Exercise 1
first :: (a -> b) -> (a, c) -> (b, c)
first f x = (f (fst x), snd x)

instance Functor Parser where
    -- fmap :: (a->b) -> Parser a -> Parser b
    fmap morphism originalParser = Parser f 
            where 
                f x = fmap (first morphism) (runParser originalParser x)

-- Exercise 2
instance Applicative Parser where
    -- pure :: a -> Parser a
    pure x = Parser f
        where f _ = Just (x, [])

    -- <*> :: Parser (a->b) -> Parser a -> Parser b
    -- p1 :: Parser (a->b)
    -- p2 :: Parser a
    (<*>) p1 p2 = Parser f 
        where f x = case runParser p1 x of
                Nothing -> Nothing
                Just (conversion, remaining) -> case runParser p2 remaining of
                    Nothing -> Nothing
                    Just (x, remaining2) -> Just (conversion x, remaining2)

type Name = String
data Employee = Emp { name :: Name, phone :: String }


-- Exercise 3
abParser :: Parser (Char, Char)
abParser = (,) <$> char 'a' <*> char 'b'

abParser_ :: Parser ()
abParser_ = (\a b -> ()) <$> char 'a' <*> char 'b'

intPair :: Parser [Integer]
intPair = (\a _ b -> [a, b])<$> posInt <*> char ' ' <*> posInt

-- Exercise 4
-- class Applicative f => Alternative f where
--     empty :: f a
--     (<|>) :: f a -> f a -> f a
instance Alternative Parser where
    empty = Parser (\_ -> Nothing)
    (<|>) p1 p2 = Parser f
        where f x = runParser p1 x <|> runParser p2 x

-- Exercise 5
intOrUppercase :: Parser ()
intOrUppercase = ((\_ -> ()) <$> posInt) <|> ((\_ -> ()) <$> satisfy isUpper)