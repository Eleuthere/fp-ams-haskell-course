module RolandParser where

import Control.Monad
import Data.Char
import Prelude hiding ((*>), (<*))

-- ****************************
-- Pre-provided
-- ****************************
newtype Parser a = Parser {runParser :: String -> Maybe (a, String)}

satisfy :: (Char -> Bool) -> Parser Char
satisfy p = Parser f
  where
    f [] = Nothing -- fail on the empty input
    f (x : xs) -- check if x satisfies the predicate
    -- if so, return x along with the remainder
    -- of the input (that is, xs)
      | p x = Just (x, xs)
      | otherwise = Nothing -- otherwise, fail

char :: Char -> Parser Char
char c = satisfy (== c)

posInt :: Parser Integer
posInt = Parser f
  where
    f xs
      | null ns = Nothing
      | otherwise = Just (read ns, rest)
      where
        (ns, rest) = span isDigit xs

-- ****************************
-- Excercise 1a

-- So -- Given a function from a to b. And a tuple of (a,c), give a tuple of (b, c).
-- Apply some f (a -> b) to the first argument of the tuple.
-- ****************************
first :: (a -> b) -> (a, c) -> (b, c)
first f (a, c) = (f a, c)

-- ****************************
-- Excercise 1b

-- Now for the functor instance...
-- fmap :: (a -> b) -> Parser a -> Parser b
-- f :: (a -> b)
-- Basically this is just fmapping the Maybe value insside the parser instance with first
-- Start by running the parser, then fmap the result,
-- By fmapping the resulting function, you actually compose the return function from the parser
-- with the function f
-- So;
-- Partially apply runParser with Parser of type A
-- Result: String -> Maybe (a, String)
-- Compose that with running (first f)
-- this needs to be mapped as the result is (-> Maybe)
-- Wrap the whole thing back up in a parser
-- fmap f parserA = Parser (fmap (first f) . runParser parserA)
--
-- Point-free:
-- fmap f = Parser . fmap (fmap (first f)) . runParser
--
-- In this case, I like the non pointfree one, it's slightly
-- better to follow than the nested fmaps
-- ****************************
instance Functor Parser where
  fmap f p = Parser $ fmap (first f) <$> runParser p

-- ****************************
-- Excercise 2

-- _ :: Parser (a -> b) -> String -> Maybe (a -> b, String)
-- This was a bit weird for me again, having to wrap my head
-- around the mapping of the function instead of the value

-- ****************************
instance Applicative Parser where
  -- pure :: a -> Parser a
  pure x = Parser (\str -> Just (x, str))

  -- (<*>) :: Parser (a -> b) -> Parser a -> Parser b
  (<*>) p1 p2 = Parser $ \str -> case runParser p1 str of
    Nothing -> Nothing
    Just (f, rest) -> runParser (fmap f p2) rest

-- ****************************
-- Excercise 3
-- ****************************
abParser :: Parser (Char, Char)
abParser = (,) <$> char 'a' <*> char 'b'

abParser_ :: Parser ()
abParser_ = (\_ _ -> ()) <$> char 'a' <*> char 'b'

-- In their example they return a list of ints, but since we're specifically
-- parsing a pair, I'd much prefer using tuples
intPairParser :: Parser (Integer, Integer)
intPairParser = (\a _ b -> (a, b)) <$> posInt <*> char ' ' <*> posInt

-- ****************************
-- Excercise 4
-- ****************************
class Applicative f => Alternative f where
  empty :: f a
  (<|>) :: f a -> f a -> f a

instance Alternative Parser where
  empty = Parser (const Nothing)
  (<|>) p1 p2 = Parser $ \str -> case runParser p1 str of
    Nothing -> runParser p2 str
    Just x -> Just x

-- ****************************
-- Excercise 5
-- ****************************
intOrUppercase :: Parser ()
intOrUppercase = void posInt <|> void (satisfy isUpper)

-- Control.Monad.void suggestion came from HLS -- nice...



-- ****************************
-- Extra -- mentioned in last meetup
--
-- Not sure if this should be called choice?
-- Basically, run both parsers, if they both 
-- succeed, chuck away the result of one of them
-- ****************************
class Applicative f => Choice f where
  (<*) :: f a -> f b -> f a
  (*>) :: f a -> f b -> f b

instance Choice Parser where
  (<*) p1 p2 = Parser $ \str -> case (runParser p1 str, runParser p2 str) of
    (Just x, Just _) -> Just x
    _ -> Nothing

  (*>) p1 p2 = Parser $ \str -> case (runParser p1 str, runParser p2 str) of
    (Just _, Just x) -> Just x
    _ -> Nothing

intPairParser' :: Parser (Integer, Integer)
intPairParser' = (,) <$> posInt <* char ' ' <*> posInt
