# Haskell: From Beginner to Intermediate #1

## FP AMS 10/02/2021 

---

# Administrivia

- Slack
- Weekly meetings
- Non-beginners

---

# [fit] Disclaimer

---

# Goal

![inline](curve.png)

---

# Overall Planning

1. Basics
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

# Books

- "Learn You a Haskell for Great Good!" by Miran Lipovača
- "Abstractions in Context" by William Yao
- "The Book of Monads" by Alejandro Serrano Mena
- "Optics by Example" by Chris Penner
- "Thinking with Types" by Sandy Maguire

---

# Why am I doing this?

- Haskell
- Learning by teaching
- (Future) colleagues

--- 

# Tools

- ~~emacs~~ text editor
- ~~compiler~~ ghc
- stack

`https://www.haskell.org/downloads/#stack`

---

# Haskell Features

- statically typed
- purely functional
- type inference
- concurrent
- lazy
- ecosystem

---

# Planning

## This Week

- Simple functions
- Lists, ranges, list comprehensions, and tuples
- Types, type variables, and type classes
- Function syntax

---

# Basics

## `ghci`

- simple arithmetic
- Boolean algebra
- testing for equality
- error messages
- function calls

---

# Basics 

## Simple Functions

```haskell
doubleMe x = x + x 
```

## Lists

```haskell
[1,2,3,4,5,6,7,8,9,10]
```

---

# Basics

## Ranges

```haskell
[first..last]
```

```haskell
[first,second..last]
```

```haskell
[first..]
```

```haskell
[first,second..]
```

---

# Basics

## Functions on Lists

- `++`
- `head`, `tail`, `last`, and `init`
- `length` and `null`
- `reverse`, `take`, and `drop`
- `maximum`, `minimum`, `sum`, and `product`
- `elem`

---

# Basics

## Functions for Creating Lists

- `repeat`
- `cycle`

---

# Basics

## List Comprehensions

```haskell
[x*2 | x <- [1..10]]
```

### With Predicates

```haskell
[x | x <- [1..10], x > 2, x < 7]
```

### With Multiple Binding Parts

```haskell
[(x,y) | x <- [1..5], y <- ['a'..'e']]
```

---

# Basics

## Tuples

```haskell
(1,'a')
```

```haskell
(1,'a',"aloha")
```


```haskell
(1,'a',"aloha",True)
```
---

# Basics

## Types

Some common types:

- `Bool`
- `Char`
- `[Char]`
- `(Integer,Char)`

---

# Basics

## Type Variables

```haskell
head :: [a] -> a
```

---

# Basics

## Type Classes

```haskell
class Eq a where
  (==) :: a -> a -> Bool
  (/=) :: a -> a -> Bool
```

## Class Constraints

```
(==) :: Eq a => a -> a -> Bool
```

---

# Basics

## Pattern Matching

```haskell
factorial :: (Integral a) => a -> a
factorial 0 = 1
factorial n = n * factorial (n - 1) 
```

---

# Basics

## Function Syntax

### Guards

```haskell
bmiTell :: (RealFloat a) => a -> a -> String  
bmiTell weight height
    | weight / height ^ 2 <= 18.5 = "underweight"
    | weight / height ^ 2 <= 25.0 = "normal"
    | weight / height ^ 2 <= 30.0 = "overweight"
    | otherwise = "obese"
```

---

# Basics

## Function Syntax

### Where Bindings

```haskell
bmiTell :: (RealFloat a) => a -> a -> String
bmiTell weight height
    | bmi <= 18.5 = "underweight"
    | bmi <= 25.0 = "normal"
    | bmi <= 30.0 = "overweight"
    | otherwise = "obese"
    where bmi = weight / height ^ 2
```
---

# Basics

## Function Syntax

### Let Bindings

```haskell
cylinder :: (RealFloat a) => a -> a -> a
cylinder r h =
    let sideArea = 2 * pi * r * h
        topArea = pi * r ^2
    in  sideArea + 2 * topArea
```

---

# Basics

## Function Syntax

### Case Expressions

```haskell
describeList :: [a] -> String
describeList xs =
  "The list is " ++ case xs of
                      [] -> "empty."
                      [x] -> "a singleton list."
                      xs -> "a longer list."
```

---

# Homework

- Set up working environment
- Play around in `ghci` with the stuff we've discussed
- Come up with a definition of "type"

---

# Planning

## Next Week

- Recursion
- Higher-order functions
- Modules
