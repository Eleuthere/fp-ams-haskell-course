# Haskell: From Beginner to Intermediate #4

## FP AMS 03/03/2021 

---

# Goal

![inline](learning-curve.png)

---

# Overall Planning

1. **Basics**
2. Functors, Applicative Functors, and Monoids
3. Monads
4. Lenses, Folds, and Traversals
5. Monad Transformers
6. Prisms and Isos
7. Type-Level Programming

---

![inline 50%](book-cover.jpg)

---

# Last Week

## Higher-Order Functions

- Currying
- Functions that take functions as parameters
- Functions that return functions
- Maps and filters
- Lambdas
- Folds
- Function application with `$`
- Function composition

---


# Homework Solutions

## Exercise 1.1

Reimplement in a more idiomatic Haskell style:

```haskell
fun1 :: [Integer] -> Integer

fun1 [] = 1
fun1 (x : xs)
  | even x = (x - 2) * fun1 xs
  | otherwise = fun1 xs
```

---

# Homework Solutions

## Exercise 1.1

Tjeerd Hans:

```haskell
fun1' :: [Integer] -> Integer
fun1' [] = 1
fun1' xs = product $ map (subtract 2) $ filter (even) xs
```

Even more idiomatic:

```haskell
fun1' :: [Integer] -> Integer
fun1' = product . map (subtract 2) . filter even
```

---

# Homework Solutions

## Exercise 1.2

Hint: For this problem you may wish to use the functions `iterate` and
`takeWhile`. Look them up in the Prelude documentation to see what they do.

```haskell
fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n
  | even n = n + fun2 (n `div` 2)
  | otherwise = fun2 (3 * n + 1)
```

---

# Homework Solutions

## Exercise 1.2

Yoshikuni:

```haskell
fun2' :: Integer -> Integer
fun2' = sum . filter even . takeWhile (/= 1) . iterate (\x -> if even x then x `div` 2 else 3 * x + 1)
```

Move clutter to a `let`:

```haskell
fun2' =
  let f n = if even n then n `div` 2 else 3 * n + 1
   in sum . filter even . takeWhile (/= 1) . iterate f
```

---

# Homework Solutions

## Exercise 3

Implement a function

```haskell
xor :: [Bool] -> Bool
```

which returns `True` if and only if there are an odd number of True values contained in the input list. It does not matter how many False values the input list contains. For example,

```haskell
xor [False, True, False] == True
xor [False, True, False, False, True] == False
```

Your solution must be implemented using a fold.

---

# This Week

## Defining Your Own Types and Typeclasses

### Part 1

- Algebraic data types

- Record syntax
- Type parameters

---

# Algebraic Data Types

Composite types:
- Product types (tuples and records)
- Sum types (tagged unions)

---

# Algebraic Data Types

## Sum Types

```haskell
data Bool = False | True
```

- keyword: `data`
- type: `Bool`
- value constructors: `False` and `True`
- note that value constructors are functions

---

# Algebraic Data Types

## Shapes (1/5)

```haskell
module Shapes (Shape (..)) where

data Shape
  = Circle Float Float Float
  | Rectangle Float Float Float Float

```

---

# Algebraic Data Types

## Shapes (2/5)

```haskell
module Shapes (Point (..), Shape (..)) where

data Point = Point Float Float
  deriving (Show)

data Shape
  = Circle Point Float
  | Rectangle Point Point
  deriving (Show)
```

---

# Algebraic Data Types

## Shapes (3/5)

```haskell
surface :: Shape -> Float
surface (Circle _ r) = pi * r ^ 2
surface (Rectangle (Point x1 y1) (Point x2 y2)) =
  abs (x2 - x1) * abs (y2 - y1)
```

---

# Algebraic Data Types

## Shapes (4/5)

```haskell
move :: Shape -> Float -> Float -> Shape
move (Circle c r) dx dy =
  Circle (move' c dx dy) r
move (Rectangle p1 p2) dx dy =
  Rectangle (move' p1 dx dy) (move' p2 dx dy)

move' :: Point -> Float -> Float -> Point
move' (Point x y) dx dy = Point (x + dx) (y + dy)
```

---

# Algebraic Data Types

## Shapes (5/5)

```haskell
baseCircle :: Float -> Shape
baseCircle r = Circle (Point 0 0) r

baseRect :: Float -> Float -> Shape
baseRect width height = Rectangle (Point 0 0) (Point width height)
```

---

# Algebraic Data Types

## Record Syntax (1/3)

Person:

- first name
- last name
- age
- height
- phone number
- favorite ice cream flavor

---

# Algebraic Data Types

## Record Syntax (2/3)

```haskell
data Person = Person String String Int Float String String deriving (Show)

firstName :: Person -> String
firstName (Person firstname _ _ _ _ _) = firstname

lastName :: Person -> String
lastName (Person _ lastname _ _ _ _) = lastname

age :: Person -> Int
age (Person _ _ age _ _ _) = age

height :: Person -> Float
height (Person _ _ _ height _ _) = height

phoneNumber :: Person -> String
phoneNumber (Person _ _ _ _ number _) = number

flavor :: Person -> String
flavor (Person _ _ _ _ _ flavor) = flavor
```

---

# Algebraic Data Types

## Record Syntax (3/3)

```haskell
data Person = Person
  { firstName :: String,
    lastName :: String,
    age :: Int,
    height :: Float,
    phoneNumber :: String,
    flavor :: String
  }
  deriving (Show)
```

---

# Type Parameters (1/2)

```haskell
data Maybe a = Nothing | Just a
```

- type parameter: `a`
- type constructor: `Maybe`
- value constructors: `Nothing` and `Just`

---

# Type Parameters (2/2)

```haskell
data Either a b = Left a | Right b
```

- type parameters: `a` and `b`
- type constructor: `Either`
- value constructors: `Left` and `Right`

---

# Next Week
  
## Defining Your Own Types and Typeclasses 

### Part 2

- Derived instances
- Type synonyms
- Recursive data structures
- Typeclasses
- Kinds

---

# Homework

Will be posted on [slack](https://join.slack.com/t/fp-ams/shared_invite/zt-mdl0ok4g-PznibhXToWSR93ilvbI_Xw)
